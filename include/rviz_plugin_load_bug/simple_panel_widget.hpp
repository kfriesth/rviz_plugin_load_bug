#ifndef SIMPLE_PANEL_WIDGET_HPP
#define SIMPLE_PANEL_WIDGET_HPP

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <rviz/panel.h>
#endif

#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QMessageBox>

namespace rviz_plugin_load_bug
{

class SimplePanelWidget : public rviz::Panel
{
Q_OBJECT
  public:
  SimplePanelWidget(QWidget* parent = 0);
  virtual ~SimplePanelWidget();

protected Q_SLOTS:
  virtual void load(const rviz::Config& config);
  virtual void save(rviz::Config config) const;
  void buttonClicked();

protected:
  QPushButton* button_;
  QLabel *label_;
};

}  // end namespace

#endif

