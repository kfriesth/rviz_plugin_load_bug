#include <rviz_plugin_load_bug/simple_panel_widget.hpp>

namespace rviz_plugin_load_bug
{

SimplePanelWidget::SimplePanelWidget(QWidget* parent) :
        rviz::Panel(parent)
{
  QVBoxLayout* layout = new QVBoxLayout();
  setLayout(layout);

  button_ = new QPushButton("Click me!");
  layout->addWidget(button_);
}

SimplePanelWidget::~SimplePanelWidget()
{
}

void SimplePanelWidget::save(rviz::Config config) const
{
  rviz::Panel::save(config);
}

void SimplePanelWidget::load(const rviz::Config& config)
{
  rviz::Panel::load(config);
  ROS_WARN_STREAM("SimplePanelWidget::load");
  label_ = new QLabel("Button connected");
  layout()->addWidget(label_);
  connect(button_, SIGNAL(clicked()), this, SLOT(buttonClicked()));
}

void SimplePanelWidget::buttonClicked()
{
  ROS_WARN_STREAM("SimplePanelWidget::buttonClicked");
  QMessageBox msg;
  msg.setText("Hello!");
  msg.exec();
}

}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(rviz_plugin_load_bug::SimplePanelWidget, rviz::Panel)
