 [![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr) RViz plugin load bug
===

[![build status](https://gitlab.com/InstitutMaupertuis/rviz_plugin_load_bug/badges/kinetic/build.svg)](https://gitlab.com/InstitutMaupertuis/rviz_plugin_load_bug/commits/kinetic)

# Launching
Source `devel/setup.bash` and launch:

```bash
roslaunch rviz_plugin_load_bug rviz.launch
```

# How to reproduce the bug

FIXME

